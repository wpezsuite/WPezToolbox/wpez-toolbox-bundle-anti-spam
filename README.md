## WPezToolbox - Bundle: Anti-spam

__ANTSPM - A WPezToolbox bundle of WordPress anti-spam plugins.__

> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Important

The plugins contained in WPezToolbox bundles are simply "you might consider these suggestions." They are not formal endorsements; there is no claim being made about their quality and/or functionality. Also, some links might be affiliate links. 


### Overview

See the README here: https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-loader


### This Bundle Includes

- https://wordpress.org/plugins/akismet/

- https://wordpress.org/plugins/antispam-bee/

- https://wordpress.org/plugins/cleantalk-spam-protect/

- https://wordpress.org/plugins/fullworks-anti-spam/

- https://wordpress.org/plugins/goodbye-captcha/

- https://wordpress.org/plugins/spam-destroyer/

- https://wordpress.org/plugins/stop-spammer-registrations-plugin/

- https://wordpress.org/plugins/zero-spam/


### Helpful Links

- https://gitlab.com/wpezsuite/WPezToolbox

- https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-loader

- https://www.wpexplorer.com/antispam-plugins-wordpress/


### TODO 



### CHANGE LOG

- v0.0.0 - 4 January 2020
   
   INIT: Hey! Ho!! Let's go!!!


