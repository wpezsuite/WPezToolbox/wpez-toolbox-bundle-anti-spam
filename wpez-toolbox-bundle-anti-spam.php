<?php
/*
Plugin Name: WPezToolbox - Bundle: Anti-spam
Plugin URI: https://gitlab.com/wpezsuite/wpeztoolbox/wpez-toolbox-bundle-anti-spam
Description: Anti-spam - A WPezToolbox bundle of WordPress Anti-spam plugins
Version: 0.0.0
Author: Mark "Chief Alchemist" Simchock for Alchemy United
Author URI: https://AlchemyUnited.com
License: GPLv2 or later
Text Domain: wpez_tbox
*/

namespace WPezToolboxBundleAntiSpam;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

add_filter( 'WPezToolboxLoader\plugins', __NAMESPACE__. '\plugins');


function plugins( $arr_in = [] ) {

	$str_bundle           = __( 'ANTSPM', 'wpez_tbox' );
	$str_prefix_separator = ' | ';
	$str_prefix           = $str_bundle . $str_prefix_separator;

	$arr = [

		/*
		'X' =>
			[
				'name'     => $str_prefix . 'X',
				'slug'     => 'X',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => '',
						'by_alt' => '',
						'url_by'  => '',
						'desc'    => '',
						'url_img' => '',
					],

					'resources' => [
						'url_wp_org' => '',
						'url_repo' => '',
						'url_site' => '',
						'url_premium' => '',
						'url_fb'   => '',
						'url_tw'   => ''
					]
				]
			],

		*/

		'akismet' =>
			[
				'name'     => $str_prefix . 'Akismet',
				'slug'     => 'akismet',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'Automattic',
						'url_by'  => 'https://automattic.com/',
						'desc'    => 'Akismet checks your comments and contact form submissions against our global database of spam to prevent your site from publishing malicious content. You can review the comment spam it catches on your blog’s “Comments” admin screen.',
						'url_img' => 'https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/akismet/',
						'url_repo' => '',
						'url_site' => 'https://akismet.com/',
						'url_premium' => 'https://akismet.com/plans/',
					]
				]
			],

		'antispam-bee' =>
			[
				'name'     => $str_prefix . 'Antispam Bee',
				'slug'     => 'antispam-bee',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'pluginkollektiv',
						'by_alt' => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/pluginkollektiv/',
						'desc'    => 'Say Goodbye to comment spam on your WordPress blog or website. Antispam Bee blocks spam comments and trackbacks effectively, without captchas and without sending personal information to third party services. It is free of charge, ad-free and 100% GDPR compliant.',
						'url_img' => 'https://ps.w.org/antispam-bee/assets/icon-128x128.png?rev=977629',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/antispam-bee/',
						'url_repo' => 'https://github.com/pluginkollektiv/antispam-bee',
						'url_site' => 'https://pluginkollektiv.org/',
					]
				]
			],

		'cleantalk-spam-protect' =>
			[
				'name'     => $str_prefix . 'Spam protection, AntiSpam, FireWall',
				'slug'     => 'cleantalk-spam-protect',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'CleanTalk',
						'url_by'  => 'https://cleantalk.org/',
						'desc'    => 'No CAPTCHA, no questions, no animal counting, no puzzles, no math and no spam bots. Universal AntiSpam plugin. Supports: Contact Form 7, Contact Form by WPForms, Ninja Forms, Gravity Forms, MailChimp, Formidable forms, WooCommerce, JetPack comments and contact form, BuddyPress, bbPress, S2Member, MailPoet, wpDiscuz, any WordPress registrations & contact forms and themes. Just setup and forget the spam!',
						'url_img' => 'https://ps.w.org/cleantalk-spam-protect/assets/icon-128x128.png?rev=1637702',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/cleantalk-spam-protect/',
						'url_repo' => 'https://github.com/CleanTalk/wordpress-antispam',
						'url_premium' => 'https://cleantalk.org/price-anti-spam',
						'url_fb'   => 'https://www.facebook.com/CleanTalk-1376135232685216/',
						'url_tw'   => 'https://twitter.com/cleantalk_en'
					]
				]
			],

		'fullworks-anti-spam' =>
			[
				'name'     => $str_prefix . 'Stop WP Comment Spam',
				'slug'     => 'fullworks-anti-spam',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'Fullworks',
						'url_by'  => 'https://fullworks.net/',
						'desc'    => 'This WordPress Anti Spam plugin detects automated comment spam to save you the constant nuisance. It does it without asking your users annoying questions, quizes or Capatcha. Simply install to start blocking spam. The free plugin is free for commercial and business sites as well as personal blogs. No API key required.',
						'url_img' => 'https://ps.w.org/fullworks-anti-spam/assets/icon-256x256.jpg?rev=2202094',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/fullworks-anti-spam/',
						'url_site' => 'https://fullworks.net/products/anti-spam/',
						'url_premium' => 'https://fullworks.net/products/anti-spam/',
						'url_fb'   => 'https://www.facebook.com/FullworksDigital/',
						'url_tw'   => 'https://twitter.com/fullworkshq'
					]
				]
			],

		'goodbye-captcha' =>
			[
				'name'     => $str_prefix . 'WPBruiser {no- Captcha anti-Spam}',
				'slug'     => 'goodbye-captcha',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'Mihai Chelaru',
						'by_alt' => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/mihche/',
						'desc'    => 'WPBruiser (formerly GoodBye Captcha) is an anti-spam and security plugin based on algorithms that identify spam bots without any annoying and hard to read captcha images. WPBruiser completely eliminates spam-bot signups, spam comments, even brute force attacks, the second you install it on your WordPress website. It is completely invisible to the end-user – no need to ever fill out a Captcha or other “human-detection” field ever again – and it just works!',
						'url_img' => 'https://ps.w.org/goodbye-captcha/assets/icon-128x128.png?rev=1356325',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/goodbye-captcha/',
						'url_site' => 'https://www.wpbruiser.com/',
						'url_premium' => 'https://www.wpbruiser.com/extensions/',
						'url_tw'   => 'https://twitter.com/WPBruiser'
					]
				]
			],


		'spam-destroyer' =>
			[
				'name'     => $str_prefix . 'Spam Destroyer',
				'slug'     => 'spam-destroyer',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'Ryan Hellyer',
						'url_by'  => 'https://geek.hellyer.kiwi/',
						'desc'    => 'Stops automated spam while remaining as unobtrusive as possible to regular commenters. The Spam Destroyer plugin is intended to be effortless to use. Simply install, and enjoy a spam free website.',
						'url_img' => 'https://ps.w.org/spam-destroyer/assets/icon-128x128.png?rev=1050320',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/spam-destroyer/',
						'url_repo' => 'https://github.com/ryanhellyer/spam-destroyer',
						'url_tw'   => 'https://twitter.com/ryanhellyer/'
					]
				]
			],

		'stop-spammer-registrations-plugin' =>
			[
				'name'     => $str_prefix . 'Stop Spammers',
				'slug'     => 'stop-spammer-registrations-plugin',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'Bryan Hadaway',
						'url_by'  => 'https://calmestghost.com/',
						'desc'    => 'Stop spam emails, spam comments, spam registration, and spam bots and spammers in general.',
						'url_img' => 'https://ps.w.org/stop-spammer-registrations-plugin/assets/icon-128x128.jpg?rev=2202880',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/stop-spammer-registrations-plugin/',
						'url_repo' => 'https://github.com/bhadaway/stop-spammers',
						'url_fb'   => 'https://www.facebook.com/calmestghost',
						'url_tw'   => 'https://twitter.com/calmestghost'
					]
				]
			],

		'zero-spam' =>
			[
				'name'     => $str_prefix . 'WordPress Zero Spam',
				'slug'     => 'zero-spam',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'Ben Marshall',
						'url_by'  => 'https://benmarshall.me/',
						'desc'    => 'WordPress Zero Spam blocks registration spam and spam in comments automatically without any additional config or setup. Just install, activate, and enjoy a spam-free site. Zero Spam was initially built based on the work by David Walsh.',
						'url_img' => 'https://ps.w.org/zero-spam/assets/icon-128x128.jpg?rev=1192239',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/zero-spam/',
						'url_repo' => 'https://github.com/bmarshall511/wordpress-zero-spam',
						'url_tw'   => 'https://twitter.com/bmarshall0511'
					]
				]
			],

	];

	$arr_mod = apply_filters( __NAMESPACE__ . '\plugins', $arr );

	if ( ! is_array( $arr_mod ) ) {
		$arr_mod = $arr;
	}

	$arr_new = array_values( $arr_mod );

	if ( is_array( $arr_in ) ) {

		return array_merge( $arr_in, $arr_new );
	}

	return $arr_new;
}